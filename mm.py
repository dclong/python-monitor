#!/usr/bin/env python
# encoding: utf-8

def xrandr():
    import subprocess
    return [line.strip() for line in subprocess.check_output(['xrandr']).strip().split("\n")]
#end def

def parse():
    raw = xrandr()
    n = len(raw)
    index = 0
    current_dev = which_device(raw[index])
    while current_dev == "":
        index += 1
        current_dev = which_device(raw[index])
    #wend
    index += 1
    devices = {}
    for i in range(index, n):
        line = raw[i]
        dev = which_device(line)
        if dev == "":
            insert(devices, current_dev, line)
        else:
            current_dev = dev
        #end if
    #end for
    return devices
#end def

def on(dev=None, resolution=None):
    import os
    import warnings
    if dev == None or dev == "":
        devices = parse()
        for dev in devices:
            on(dev, devices[dev])
        #end for
        return
    #end if
    if type(dev) == list:
        n = len(dev)
        if n == 0:
            on()
            return
        #end if
        if n == 1:
            dev = dev[0].upper()
            devices = parse()
            if devices.has_key(dev):
                on(dev, devices[dev])
                return
            #end if
            warnings.warn("Unrecognized device!")
            return
        #end if
        if n == 2:
            on(dev[0], dev[1])
            return
        #end if
        warnings.warn("Wrong number of arguments!")
        return
    #end if
    if type(resolution) == list:
        for res in resolution:
            if res[0]:
                on(dev, res[1])
                return
            #end if
        #end for
        res = resolution[0]
        on(dev, res[1])
    #end if
    dev = dev.upper()
    cmd = 'xrandr --output ' + dev + ' --mode ' + resolution
    os.system(cmd)
    print(dev + " has been turned on at resolution " + resolution + ".")
#end def

def off(dev=None):
    import os
    import warnings
    if dev == None or dev == "":
        devices = parse()
        for dev in devices:
            if dev != 'LVDS1':
                off(dev)
            #end if
        #end for
        return
    #end if
    if type(dev) == list:
        n = len(dev)
        if n == 0:
            off()
            return
        #end if
        if n == 1:
            off(dev[0])
            return
        #end if
        warnings.warn("Too many arguments!")
        return
    #end if
    dev = dev.upper()
    cmd = 'xrandr --output ' + dev + ' --off'
    os.system(cmd)
    print(dev + " has been turned off.")
#end def

def show():
    import os
    os.system('xrandr')
#end def

def insert(devices, dev, line):
    default = line.rfind('+') >= 0
    line = line.split(" ")
    resolution = line[0]
    if not devices.has_key(dev):
        devices[dev] = []
    #end if
    devices[dev].append((default, resolution))
#end def

def which_device(line):
    if line.find("connected") >= 0:
        return line[0:line.find(" ")]
    #end if
    return ""
#end def

def run_cmd(cmdsp):
    import warnings
    funs_without_args = ["show"]
    funs_with_args = ["on", "off"]
    fun = cmdsp[0]
    if fun in funs_without_args:
        eval(fun + "()")
        return
    #end if  
    if fun in funs_with_args:
        cmdsp.pop(0)
        eval(fun + "(cmdsp)")
        return
    #end if
    warnings.warn("Command " + fun + " is not recognized.")
#end 

if __name__ == '__main__':
    import sys
    sys.argv.pop(0)
    run_cmd(sys.argv)
#end if
